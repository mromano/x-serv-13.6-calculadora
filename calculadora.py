import sys
import decimal

def suma(A,B):
	return A+B
def resta(A,B):
	return A-B
def multiplicacion(A,B):
	return A*B
def division(A,B):
    if B == 0:
        print("Error: division by 0 is not allowed")
        exit()
    else:
        return A/B


if len(sys.argv) != 4:
    print("Usage Error: <operation> <number1> <number2>")
    exit()
else:
    operation = sys.argv[1]

    try:
        op1 = decimal.Decimal(sys.argv[2])
        op2 = decimal.Decimal(sys.argv[3])
    except decimal.InvalidOperation:
        print("Error: operands must be decimal numbers")
        exit()

    if operation == "sumar":
        print(suma(op1, op2))
    elif operation == "restar":
        print(resta(op1, op2))
    elif operation == "multiplicar":
        print(multiplicacion(op1, op2))
    elif operation == "dividir":
        print(division(op1, op2))
    else:
        print("Error: invalid operation")
        print("Try: <sumar> <restar> <multiplicar> <dividir>")

        
